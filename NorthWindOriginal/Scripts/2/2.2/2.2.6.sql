﻿USE [Northwind]
GO

SELECT employ1.EmployeeID as 'Employee', employ2.EmployeeID as 'ReportTo'
FROM dbo.Employees as employ1, dbo.Employees as employ2
WHERE employ1.ReportsTo = employ2.EmployeeID
