﻿USE [Northwind]
GO

SELECT COUNT(
CASE
	WHEN ord.[ShippedDate] is null THEN 1
END
) as 'Shipped Date'
FROM dbo.[Orders] as ord

