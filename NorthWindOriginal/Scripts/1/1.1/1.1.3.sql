﻿USE [Northwind]
GO

SELECT ord.[OrderID] as 'Order Number', 
CASE
	When ord.ShippedDate is NULL  THEN 'Not shipped'
	ELSE CONVERT(nvarchar, ord.ShippedDate)
END AS 'Shipped Date'
FROM dbo.[Orders] as ord
WHERE ord.ShippedDate > '1998-05-06' OR ord.ShippedDate is NULL