﻿USE [Northwind]
GO

SELECT 
	(
	SELECT CONCAT(emp.[FirstName], ' ', emp.[LastName]) 
	FROM dbo.[Employees] as emp
	WHERE emp.[EmployeeID] = ord.[EmployeeID]
	) as [Seller], COUNT(ord.[EmployeeID]) as [Amount]
FROM dbo.[Orders] as ord
GROUP BY ord.[EmployeeID]
ORDER BY [Amount] DESC
