﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE [id] = OBJECT_ID('dbo.CreditCards'))
BEGIN
	:r .\3.1.sql
END

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE [id] = OBJECT_ID('dbo.Region'))
BEGIN
	EXEC SP_RENAME 'dbo.Region', 'dbo.Regions' 
END

IF COL_LENGTH('dbo.Customers', 'DateOfFoundation') IS NULL
BEGIN
	ALTER TABLE dbo.[Customers]
	ADD DateOfFoundation DateTime
END
