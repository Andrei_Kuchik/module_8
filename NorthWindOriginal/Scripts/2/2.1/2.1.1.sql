﻿USE [Northwind]
GO

SELECT SUM(ordt.[UnitPrice] * ordt.[Quantity] - ordt.[Quantity] * ordt.[Discount]) as [Totals]
FROM dbo.[Order Details] as ordt