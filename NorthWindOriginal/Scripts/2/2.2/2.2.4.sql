﻿USE [Northwind]
GO

SELECT DISTINCT cust.[City], COUNT([CustomerID]) as [Count persons]
FROM dbo.[Customers] as cust, dbo.[Employees] as emp
WHERE cust.[City] = emp.[City]
GROUP BY cust.[City], emp.[City] 
HAVING COUNT([CustomerID]) > 1 
