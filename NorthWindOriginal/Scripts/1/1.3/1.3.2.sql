﻿USE [Northwind]
GO

SELECT cust.[CustomerID], cust.[Country]
FROM dbo.[Customers] as cust
WHERE LEFT(cust.[Country], 1) BETWEEN 'b' AND 'g'
ORDER BY cust.[Country] 