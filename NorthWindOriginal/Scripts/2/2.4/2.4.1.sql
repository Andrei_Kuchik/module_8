﻿USE [Northwind]
GO

SELECT sup.[CompanyName]
FROM [Suppliers] as sup
JOIN 
(
	SELECT * 
	FROM dbo.[Products] as prod
	WHERE prod.[UnitsInStock] = 0 
) as prod ON prod.[SupplierID] = sup.[SupplierID]
