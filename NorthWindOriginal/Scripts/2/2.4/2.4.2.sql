﻿USE [Northwind]
GO

SELECT emp.[FirstName], emp.[LastName], ord.[Count Orders]
FROM dbo.[Employees] as emp
JOIN 
(
	SELECT ord.[EmployeeID], COUNT(*) as [Count Orders] 
	FROM dbo.[Orders] as ord
	GROUP BY ord.[EmployeeID]
	HAVING COUNT(*) > 150
) as ord ON ord.[EmployeeID] = emp.[EmployeeID]