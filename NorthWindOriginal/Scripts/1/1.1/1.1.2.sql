﻿USE [Northwind]
GO

SELECT ord.OrderID, 
	'Not shipped' as ShippedDate
	-- I don't understand why use "Case"
--CASE
--	When ord.ShippedDate is NULL  THEN 'Not shipped'
--	ELSE CONVERT(varchar, ord.ShippedDate)
--END AS ShippedDate
FROM dbo.Orders as ord
WHERE ord.ShippedDate is NULL
